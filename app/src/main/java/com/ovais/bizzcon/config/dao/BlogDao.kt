package com.ovais.bizzcon.config.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ovais.bizzcon.data.blogs.Data

/**
Created By Syed Ovais Akhtar On 4/30/21 2:18 PM2
 **/

@Dao
interface BlogDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(blogs: List<Data>)

    @Query("SELECT * FROM blogs ORDER BY blog_id DESC")
    fun getAllBlog(): List<Data>
}