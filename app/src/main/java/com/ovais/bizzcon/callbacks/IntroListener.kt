package com.ovais.bizzcon.callbacks

/**
Created By Syed Ovais Akhtar On 4/18/21 12:31 AM2
 **/
interface IntroListener {
    fun onFinished()
}