package com.ovais.bizzcon.app

import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseActivity
import com.ovais.bizzcon.databinding.ActivityAppBinding
import com.ovais.bizzcon.preferences.PreferenceHelper
import com.ovais.bizzcon.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.Subscribe

@AndroidEntryPoint
class AppActivity : BaseActivity<ActivityAppBinding>() {

    override fun getLayout(): Int = R.layout.activity_app

    override fun onPostInit() = Unit

    @Subscribe
    fun onLogoutEvent() {
        PreferenceHelper.clearLoggedInPreferences()
        showToast(getString(R.string.logging_out))
        finish()
    }

}