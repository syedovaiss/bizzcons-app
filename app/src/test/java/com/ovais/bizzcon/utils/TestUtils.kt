package com.ovais.bizzcon.utils

import com.ovais.bizzcon.base.BaseErrorResponse
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.data.user.User
import com.ovais.bizzcon.requestbody.TokenRequestBody
import java.io.File

/**
Created By Syed Ovais Akhtar On 5/25/21 3:05 PM2
 **/
object TestUtils {

    val blog = Data(
        1,
        "Test Blog Title",
        "Test Sub Title",
        "Description",
        "https://www.google.com.pk",
        "12/12/12",
        "12/12/12"
    )

    val project = com.ovais.bizzcon.data.projects.Data(
        1, "Test Blog Title",
        "Test Sub Title",
        "Description",
        "12/12/12",
        "12/12/12"
    )

    val user = User(
        "Syed Ovais",
        "ovais.hussain@venturedive.com",
        "Ovais123",
        "03362402603",
        "Precinct 10-A Bahria Town Karachi",
        "01-July-1997",
        File("Ovais.png")
    )

    val tokenRequestBody = TokenRequestBody(user.email!!, user.password!!)
}

