package com.ovais.bizzcon.data.user

data class Data(
    val address: String,
    val dateOfBirth: String,
    val email: String,
    val fullname: String,
    val phone: String,
    val profileImage: String,
    val userID: Int
)