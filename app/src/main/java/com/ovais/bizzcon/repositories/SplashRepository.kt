package com.ovais.bizzcon.repositories

import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.data.blogs.Blogs
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.datasource.SplashDataSource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 3/31/21 7:00 PM2
 **/
class SplashRepository @Inject constructor(private val dataSource: SplashDataSource)  {

    suspend fun getBlog() = flow {
        emit(dataSource.getBlogs())
    }

    suspend fun getProjects() = flow{
        emit(dataSource.getProjects())
    }

    suspend fun getServices() = flow {
        emit(dataSource.getServices())
    }

    suspend fun insertBlog(blog: List<Data>) = flow {
        emit(dataSource.insertBlog(blog))
    }

    suspend fun insertProjects(projects: List<com.ovais.bizzcon.data.projects.Data>) =  flow{
        emit(dataSource.insertProjects(projects))
    }
}