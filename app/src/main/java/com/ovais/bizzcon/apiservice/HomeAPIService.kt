package com.ovais.bizzcon.apiservice

import com.ovais.bizzcon.data.blogs.Blogs
import com.ovais.bizzcon.data.projects.Projects
import retrofit2.Response
import retrofit2.http.GET

/**
Created By Syed Ovais Akhtar On 4/23/21 12:20 AM2
 **/
interface HomeAPIService {

    @GET("api/blogs/all/1")
    suspend fun getRecentBlog() : Response<Blogs>

    @GET("api/projects/all/1")
    suspend fun getRecentProjects() : Response<Projects>
}