package com.ovais.bizzcon.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
Created By Syed Ovais Akhtar On 3/21/21 11:07 PM2
 **/
abstract class BaseActivity<B : ViewDataBinding> : AppCompatActivity() {

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayout())
        onPostInit()
    }

    abstract fun getLayout(): Int


    abstract fun onPostInit()
}