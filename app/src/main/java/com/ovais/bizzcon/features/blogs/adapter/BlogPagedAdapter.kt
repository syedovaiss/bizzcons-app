package com.ovais.bizzcon.features.blogs.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ovais.bizzcon.callbacks.OnBlogTapListener
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.databinding.ItemRowsHorizontalBinding
import com.ovais.bizzcon.utils.applyImage
import com.ovais.bizzcon.utils.value

/**
Created By Syed Ovais Akhtar On 5/24/21 4:21 PM2
 **/
class BlogPagedAdapter(val onBlogTapListener: OnBlogTapListener): PagingDataAdapter<Data, BlogPagedAdapter.BlogViewHolder>(BLOG_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogViewHolder {
        val binding = ItemRowsHorizontalBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return BlogViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BlogViewHolder, position: Int) {
        val currentItem = getItem(position)

        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

    inner class BlogViewHolder(private val binding: ItemRowsHorizontalBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(blog: Data) {
            binding.apply {
                image.applyImage(binding.root.context,blog.blog_header_image)
                description.value(blog.blog_sub_title)
                title.value(blog.blog_title)
                layout.setOnClickListener {
                    onBlogTapListener.onBlogTapped(blog)
                }

            }


        }
    }

    companion object {
        private val BLOG_COMPARATOR = object : DiffUtil.ItemCallback<Data>() {

            override fun areItemsTheSame(oldItem: Data, newItem: Data) = oldItem.blog_id==newItem.blog_id

            override fun areContentsTheSame(oldItem: Data, newItem: Data) = oldItem == newItem
        }
    }
}