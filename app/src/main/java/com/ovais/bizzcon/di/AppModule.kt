package com.ovais.bizzcon.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ovais.bizzcon.apiservice.*
import com.ovais.bizzcon.app.BizzconsApplication
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.config.network.NetworkClient
import com.ovais.bizzcon.datasource.*
import com.ovais.bizzcon.repositories.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
Created By Syed Ovais Akhtar On 3/31/21 6:49 PM2
 **/

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun providesRetrofit(gson: Gson) = NetworkClient.getInstance(gson)

    @Provides
    fun providesGson(): Gson = GsonBuilder().create()

    @Provides
    fun getContext(): Context = BizzconsApplication.context!!

    @Provides
    fun provideDb(context: Context) = BizzconsDatabase(context)

    @Provides
    fun getGeneralAPIService(retrofit: Retrofit): GeneralClient = retrofit.create(GeneralClient::class.java)

    @Provides
    fun getSplashDataSource(apiService: GeneralClient, database: BizzconsDatabase) = SplashDataSource(apiService, database)

    @Provides
    fun getSplashRepository(dataSource: SplashDataSource) = SplashRepository(dataSource)


    //User(works on register and login)

    @Provides
    fun getUserAPIService(retrofit: Retrofit): UserAPIService = retrofit.create(UserAPIService::class.java)

    @Provides
    fun getUserDataSource(apiService: UserAPIService) = UserRemoteDataSource(apiService)

    @Provides
    fun getUserRepository(dataSource: UserRemoteDataSource) = UserRepository(dataSource)

    //Home

    @Provides
    fun getHomeAPIService(retrofit: Retrofit): HomeAPIService = retrofit.create(HomeAPIService::class.java)

    @Provides
    fun getHomeDataSource(apiService: HomeAPIService) = HomeDataSource(apiService)

    @Provides
    fun getHomeRepository(dataSource: HomeDataSource) = HomeRepository(dataSource)

    //Blog

    @Provides
    fun getBlogApiService(retrofit: Retrofit): BlogApiService = retrofit.create(BlogApiService::class.java)

    @Provides
    fun getBlogDataSource(apiService: BlogApiService,db:BizzconsDatabase) = BlogDataSource(apiService,db)


    @Provides
    fun getBlogPagingSource(apiService: BlogApiService) = BlogPagingSource(apiService)

    @Provides
    fun getBlogRepository(dataSource: BlogDataSource,pagingDataSource:BlogPagingSource) = BlogRepository(dataSource,pagingDataSource)

    //Project

    @Provides
    fun getProjectApiService(retrofit: Retrofit): ProjectApiService = retrofit.create(ProjectApiService::class.java)

    @Provides
    fun getProjectDataSource(apiService: ProjectApiService,db:BizzconsDatabase) = ProjectDataSource(apiService,db)

    @Provides
    fun getProjectPagedSource(apiService: ProjectApiService) = ProjectPagedSource(apiService)

    @Provides
    fun getProjectRepository(dataSource: ProjectDataSource,pagingDataSource:ProjectPagedSource) = ProjectRepository(dataSource,pagingDataSource)

}