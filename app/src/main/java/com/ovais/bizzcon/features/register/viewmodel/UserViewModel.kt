package com.ovais.bizzcon.features.register.viewmodel

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.data.common.GeneralResponse
import com.ovais.bizzcon.data.token.TokenResponse
import com.ovais.bizzcon.data.user.User
import com.ovais.bizzcon.data.user.UserResponse
import com.ovais.bizzcon.repositories.UserRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
Created By Syed Ovais Akhtar On 4/18/21 12:38 AM2
 **/
class UserViewModel @ViewModelInject constructor(private var repository: UserRepository) :
    ViewModel(), Observable {

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {}

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {}

    private var _registerResponse = MutableLiveData<Resource<GeneralResponse>>()

    val registerResponse: LiveData<Resource<GeneralResponse>> get() = _registerResponse

    private val _tokenRequest = MutableLiveData<Resource<TokenResponse>>()

    @Bindable
    var email = MutableLiveData<String>()

    @Bindable
    var password = MutableLiveData<String>()

    val tokenResponse: LiveData<Resource<TokenResponse>> get() = _tokenRequest

    private var _errorResponse = MutableLiveData<String>()
    val errorResponse = _errorResponse

    private var _userInfo = MutableLiveData<Resource<UserResponse>>()
    val userInfo: LiveData<Resource<UserResponse>> get() = _userInfo

    fun registerUser(user: User?) {
        viewModelScope.launch {
            repository.registerUser(user!!).collect {
                _registerResponse.value = it
            }
        }
    }

    fun enqueueTokenRequest(email: String, password: String) {
        when {
            email.isBlank() -> {
                _errorResponse.value = "Please enter email address"
            }
            password.isBlank() -> {
                errorResponse.value = "Please enter password"
            }
            else -> {
                viewModelScope.launch {
                    repository.enqueueTokenRequest(email,password).collect {
                        _tokenRequest.value = it
                    }
                }

            }
        }


    }

    fun getUserInfo() {
        viewModelScope.launch {
            repository.getUserInfo().collect {
                _userInfo.value = it
            }
        }
    }

}