package com.ovais.bizzcon.features.blogs.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.repositories.BlogRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/20/21 10:30 PM2
 **/
class BlogViewModel @ViewModelInject constructor(private val repository: BlogRepository) : ViewModel() {


    private var currentSearchResult: Flow<PagingData<Data>>? = null


    fun getPaginatedBlog(): Flow<PagingData<Data>>? {

        val newResult: Flow<PagingData<Data>> = repository.getPaginatedBlog()
            .cachedIn(viewModelScope)
        currentSearchResult = newResult
        return currentSearchResult

    }


}