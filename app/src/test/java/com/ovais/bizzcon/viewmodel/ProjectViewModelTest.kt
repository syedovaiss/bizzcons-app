package com.ovais.bizzcon.viewmodel

import android.content.Context
import androidx.paging.PagingData
import com.google.common.truth.Truth
import com.ovais.bizzcon.apiservice.ProjectApiService
import com.ovais.bizzcon.config.dao.ProjectDao
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.data.projects.Data
import com.ovais.bizzcon.datasource.ProjectDataSource
import com.ovais.bizzcon.datasource.ProjectPagedSource
import com.ovais.bizzcon.features.projects.viewmodel.ProjectViewModel
import com.ovais.bizzcon.repositories.ProjectRepository
import com.ovais.bizzcon.utils.TestUtils
import com.ovais.bizzcon.utils.getDatabase
import com.ovais.bizzcon.utils.getProjectDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
Created By Syed Ovais Akhtar On 5/25/21 4:24 PM2
 **/
@ExperimentalCoroutinesApi
class ProjectViewModelTest {

    private lateinit var repository: ProjectRepository
    private lateinit var dataSource: ProjectDataSource
    private lateinit var pagingDataSource: ProjectPagedSource

    @Mock
    private lateinit var apiService: ProjectApiService

    @Mock
    private lateinit var context: Context
    private lateinit var db: BizzconsDatabase
    private lateinit var projectDao: ProjectDao
    private lateinit var viewModel: ProjectViewModel


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        setupDatabase()
        dataSource = ProjectDataSource(apiService, db)
        pagingDataSource = ProjectPagedSource(apiService)
        repository = ProjectRepository(dataSource, pagingDataSource)
        viewModel = ProjectViewModel(repository)

    }

    private fun setupDatabase() {
        db = getDatabase(context)
        projectDao = getProjectDao(db)
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun `get paginated blog`() = runBlocking {
        val data = TestUtils.project
        val listOfProjects = listOf(data, data, data, data)
        val flow = flow<PagingData<List<Data>>> {}
        flow.collect {
            Truth.assertThat(repository.getPaginatedProjects()).isEqualTo(listOfProjects)

        }
    }
}