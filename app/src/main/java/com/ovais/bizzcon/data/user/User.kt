package com.ovais.bizzcon.data.user

import java.io.File
import java.io.Serializable

/**
Created By Syed Ovais Akhtar On 4/20/21 4:36 PM2
 **/
data class User(

    var fullName: String? = null,
    var email: String? = null,
    var password: String? = null,
    var phone: String? = null,
    var address: String? = null,
    var dateOfBirth: String? = null,
    var profile: File? = null
):Serializable