package com.ovais.bizzcon.features.login.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.databinding.FragmentLoginBinding
import com.ovais.bizzcon.features.register.viewmodel.UserViewModel
import com.ovais.bizzcon.preferences.PreferenceHelper
import com.ovais.bizzcon.utils.*
import com.ovais.bizzcon.utils.Constants.PREFERENCES.IS_LOGGED_IN
import com.ovais.bizzcon.utils.Constants.PREFERENCES.USER_DETAILS
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 4/18/21 12:45 AM2
 **/
@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>() {


    private val viewModel: UserViewModel by viewModels()

    @Inject
    lateinit var gson: Gson

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentLoginBinding = FragmentLoginBinding.inflate(inflater, container, false)

    override fun onPostInit() {
        routeToRegisterFragment()
        setupListeners()
        setupLoginObserver()
        setupUserInfoObserver()
        setupLoginErrorObserver()

    }

    private fun setupListeners() {
        binding.loginButton.setOnClickListener {
            val email = binding.emailET.value()
            val password = binding.passwordET.value()
            viewModel.enqueueTokenRequest(email,password)
        }
    }

    private fun setupLoginObserver() {
        viewModel.tokenResponse.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressLoader.hide()
                    PreferenceHelper.putToken(it.data?.data!!.token)
                    viewModel.getUserInfo()
                }
                Resource.Status.ERROR -> {
                    binding.progressLoader.hide()
                    showToast(it.responseError!!.error)
                }
                Resource.Status.LOADING -> binding.progressLoader.show()
            }
        })
    }


    private fun routeToRegisterFragment() {
        binding.registerTextView.setOnClickListener {
            routeTo(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun setupUserInfoObserver() {
        viewModel.userInfo.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    val userJson = gson.toJson(it.data?.data)
                    PreferenceHelper.putString(USER_DETAILS, userJson)
                    binding.progressLoader.hide()
                    PreferenceHelper.putBoolean(IS_LOGGED_IN, true)
                    routeTo(R.id.action_loginFragment_to_homeFragment)
                }
                Resource.Status.ERROR -> {
                    binding.progressLoader.hide()
                    showToast(it.data?.error!!)
                }
                Resource.Status.LOADING -> binding.progressLoader.show()

            }
        })
    }

    private fun setupLoginErrorObserver() {
        viewModel.errorResponse.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })
    }
}