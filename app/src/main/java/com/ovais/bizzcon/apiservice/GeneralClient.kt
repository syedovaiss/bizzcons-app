package com.ovais.bizzcon.apiservice

import com.ovais.bizzcon.data.blogs.Blogs
import com.ovais.bizzcon.data.projects.Projects
import com.ovais.bizzcon.data.services.Services
import com.ovais.bizzcon.utils.Constants.NETWORK_CONSTANTS.BLOG_ENDPOINT
import com.ovais.bizzcon.utils.Constants.NETWORK_CONSTANTS.PROJECTS_ENDPOINT
import com.ovais.bizzcon.utils.Constants.NETWORK_CONSTANTS.SERVICES_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

/**
Created By Syed Ovais Akhtar On 3/31/21 7:29 PM2
 **/

interface GeneralClient {

    //getting just page 1  for now

    @GET(BLOG_ENDPOINT)
    suspend fun getBlog(): Response<Blogs>

    @GET(PROJECTS_ENDPOINT)
    suspend fun getProjects(): Response<Projects>

    @GET(SERVICES_ENDPOINT)
    suspend fun getServices():Response<Services>
}