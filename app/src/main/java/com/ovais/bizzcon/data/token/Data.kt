package com.ovais.bizzcon.data.token

data class Data(
    val expirationTime: String,
    val token: String
)