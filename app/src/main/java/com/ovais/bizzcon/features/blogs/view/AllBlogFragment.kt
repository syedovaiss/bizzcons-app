package com.ovais.bizzcon.features.blogs.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.callbacks.OnBlogTapListener
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.databinding.FragmentAllBlogBinding
import com.ovais.bizzcon.datasource.BlogDataSource
import com.ovais.bizzcon.datasource.BlogPagingSource
import com.ovais.bizzcon.features.blogs.adapter.BlogPagedAdapter
import com.ovais.bizzcon.features.blogs.viewmodel.BlogViewModel
import com.ovais.bizzcon.repositories.BlogRepository
import com.ovais.bizzcon.utils.hide
import com.ovais.bizzcon.utils.init
import com.ovais.bizzcon.utils.routeTo
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/20/21 5:27 PM2
 **/
@AndroidEntryPoint
class AllBlogFragment : BaseFragment<FragmentAllBlogBinding>() ,OnBlogTapListener{

    private val viewModel: BlogViewModel by viewModels()
    private lateinit var blogPagedAdapter: BlogPagedAdapter

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAllBlogBinding = FragmentAllBlogBinding.inflate(inflater, container, false)

    override fun onPostInit() {
        setupAdapter()
        setupData()
    }

    private fun setupData() {
        lifecycleScope.launch {
            viewModel.getPaginatedBlog()?.let {
                it.collectLatest {
                    binding.progressBar.hide()
                    blogPagedAdapter.submitData(it)
                }
            }
        }
        setupRecyclerView()
    }

    private fun setupAdapter() {
        blogPagedAdapter = BlogPagedAdapter(this)
    }

    private fun setupRecyclerView() {
        binding.allBlogRecyclerView.init(requireContext(),blogPagedAdapter)

    }

    override fun onBlogTapped(data: Data) {
        routeTo(R.id.action_allBlogFragment_to_blogDetailFragment, bundleOf("blog" to data))

    }
}