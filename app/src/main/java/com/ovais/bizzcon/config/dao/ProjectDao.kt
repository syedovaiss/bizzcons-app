package com.ovais.bizzcon.config.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ovais.bizzcon.data.projects.Data

/**
Created By Syed Ovais Akhtar On 4/30/21 2:18 PM2
 **/

@Dao
interface ProjectDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(projects: List<com.ovais.bizzcon.data.projects.Data>)

    @Query("SELECT * FROM projects ORDER BY project_id")
    fun getAllProjects(): List<Data>
}