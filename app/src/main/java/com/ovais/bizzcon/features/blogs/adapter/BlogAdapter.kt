package com.ovais.bizzcon.features.blogs.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ovais.bizzcon.R
import com.ovais.bizzcon.callbacks.OnBlogTapListener
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.databinding.ItemRowsBinding
import com.ovais.bizzcon.utils.value
import kotlinx.android.synthetic.main.item_rows.view.*

/**
Created By Syed Ovais Akhtar On 4/29/21 8:22 PM2
 **/
class BlogAdapter(private var listener: OnBlogTapListener) :
    RecyclerView.Adapter<BlogAdapter.BlogViewHolder>() {

    private var listItems = ArrayList<Data>()

    fun addBlogs(items: ArrayList<Data>) {
        this.listItems = items
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogViewHolder {
        val view = ItemRowsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BlogViewHolder(view)

    }

    override fun onBindViewHolder(holder: BlogViewHolder, position: Int) {
        val items = listItems[position]
        holder.bindData(items)

    }

    override fun getItemCount(): Int = listItems.size


    inner class BlogViewHolder(var itemRows: ItemRowsBinding) :
        RecyclerView.ViewHolder(itemRows.root) {

        fun bindData(items: Data) {
            Glide.with(itemView.context)
                .load(items.blog_header_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.placeholder)
                .into(itemView.image)
            itemRows.title.value(items.blog_title)
            itemRows.layout.setOnClickListener {
                listener.onBlogTapped(items)
            }

        }


    }

}