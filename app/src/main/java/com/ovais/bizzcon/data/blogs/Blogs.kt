package com.ovais.bizzcon.data.blogs

data class Blogs(
    val `data`: List<Data>,
    val error: String,
    val success: Boolean,
    val nextPageNo:Int
)