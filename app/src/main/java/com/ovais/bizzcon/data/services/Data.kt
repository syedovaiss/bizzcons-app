package com.ovais.bizzcon.data.services

data class Data(
    val createdAt: String,
    val service_description: String,
    val service_id: Int,
    val service_title: String,
    val service_type: String,
    val updatedAt: String
)