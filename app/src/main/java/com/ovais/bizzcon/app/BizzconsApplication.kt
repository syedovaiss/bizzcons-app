package com.ovais.bizzcon.app

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.work.*
import com.google.android.gms.ads.MobileAds
import com.ovais.bizzcon.BuildConfig
import com.ovais.bizzcon.preferences.PreferenceHelper
import com.ovais.bizzcon.utils.Constants.WORKER.TAG_BLOG
import com.ovais.bizzcon.utils.Constants.WORKER.TAG_PROJECTS
import com.ovais.bizzcon.worker.BlogWorker
import com.ovais.bizzcon.worker.ProjectWorker
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
Created By Syed Ovais Akhtar On 3/21/21 11:04 PM2
 **/
@HiltAndroidApp
class BizzconsApplication : Application() {

    private lateinit var workManager: WorkManager

    override fun onCreate() {
        super.onCreate()
        PreferenceHelper.getInstance(this)
        workManager = WorkManager.getInstance(this)
        context = this
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        enqueueWorker()
        initializeAds()
    }

    private fun initializeAds(){
        MobileAds.initialize(this)
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null
    }


    private fun enqueueWorker() {
        workManager.enqueue(getBlogWorkRequest())
        workManager.enqueue(getProjectsWorkRequest())
    }

    private fun getWorkConstraints(): Constraints {

        return Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
    }

    private fun getBlogWorkRequest(): PeriodicWorkRequest {

        return PeriodicWorkRequestBuilder<BlogWorker>(15, TimeUnit.MINUTES)
            .addTag(TAG_BLOG)
            .setConstraints(getWorkConstraints())
            .setBackoffCriteria(
                BackoffPolicy.LINEAR,
                PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                TimeUnit.MILLISECONDS
            )
            .build()
    }

    private fun getProjectsWorkRequest(): PeriodicWorkRequest {

        return PeriodicWorkRequestBuilder<ProjectWorker>(15, TimeUnit.MINUTES)
            .addTag(TAG_PROJECTS)
            .setConstraints(getWorkConstraints())
            .setBackoffCriteria(
                BackoffPolicy.LINEAR,
                PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                TimeUnit.MILLISECONDS
            )
            .build()
    }
}