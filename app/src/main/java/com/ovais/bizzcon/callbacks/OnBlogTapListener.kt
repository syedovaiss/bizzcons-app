package com.ovais.bizzcon.callbacks

import com.ovais.bizzcon.data.blogs.Data

/**
Created By Syed Ovais Akhtar On 4/29/21 8:23 PM2
 **/
interface OnBlogTapListener {
    fun onBlogTapped(data: Data)
}