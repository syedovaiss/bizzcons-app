package com.ovais.bizzcon.features.home.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.gson.Gson
import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.callbacks.OnBlogTapListener
import com.ovais.bizzcon.callbacks.OnProjectClickListener
import com.ovais.bizzcon.callbacks.ServiceListener
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.databinding.FragmentHomeBinding
import com.ovais.bizzcon.datasource.HomeDataSource
import com.ovais.bizzcon.features.blogs.adapter.BlogAdapter
import com.ovais.bizzcon.features.home.viewmodel.HomeViewModel
import com.ovais.bizzcon.features.projects.adapter.ProjectAdapter
import com.ovais.bizzcon.features.services.adapter.ServiceAdapter
import com.ovais.bizzcon.features.services.model.Services
import com.ovais.bizzcon.preferences.PreferenceHelper
import com.ovais.bizzcon.repositories.HomeRepository
import com.ovais.bizzcon.utils.Constants.PREFERENCES.USER_DETAILS
import com.ovais.bizzcon.utils.routeTo
import com.ovais.bizzcon.utils.setGreetingText
import com.ovais.bizzcon.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 4/23/21 12:17 AM2
 **/
@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(), ServiceListener,
    OnBlogTapListener, OnProjectClickListener {

    private lateinit var serviceAdapter: ServiceAdapter
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var blogAdapter: BlogAdapter
    private lateinit var projectAdapter: ProjectAdapter

    @Inject
    lateinit var gson: Gson

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false)

    override fun onPostInit() {
        setupAds()
        setupUserInfo()
        setupAdapter()
        setupServices()
        setupBlogs()
        setupProjects()
        routeToAllBlog()
        routeToAllProjects()
        setupLogoutListeners()
    }

    private fun setupAds() {
        val adRequest = AdRequest.Builder().build()
        binding.adView.loadAd(adRequest)
    }

    private fun setupUserInfo() {
        val userJSON = PreferenceHelper.getString(USER_DETAILS)
        val userDetails = gson.fromJson(userJSON, com.ovais.bizzcon.data.user.Data::class.java)
        binding.headerText.setGreetingText(userDetails.fullname)

    }

    private fun setupLogoutListeners(){
        binding.logout.setOnClickListener {
            PreferenceHelper.clearLoggedInPreferences()
            showToast(getString(R.string.logging_out))
            routeTo(R.id.action_homeFragment_to_loginFragment)
        }
    }
    private fun setupAdapter() {
        serviceAdapter = ServiceAdapter(this)
        blogAdapter = BlogAdapter(this)
        projectAdapter = ProjectAdapter(this)

    }

    private fun setupBlogs() {
        viewModel.requestBlog()
        viewModel.recentBlog.observe(viewLifecycleOwner, Observer {
            if (it.status == Resource.Status.SUCCESS) {
                blogAdapter.addBlogs(it.data?.data as ArrayList<Data>)
                setupBlogRecyclerView()
                hideProgressLoader(binding.progressBar)

            }
            if (it.status == Resource.Status.LOADING) {
                showProgressLoader(binding.progressBar)

            }
            if (it.status == Resource.Status.ERROR) {
                hideProgressLoader(binding.progressBar)
                showToast(it.message.toString())
            }

        })
    }

    private fun setupProjects() {
        viewModel.requestProjects()
        viewModel.recentProjects.observe(viewLifecycleOwner, Observer {
            if (it.status == Resource.Status.SUCCESS) {
                projectAdapter.addProjects(it.data?.data as ArrayList<com.ovais.bizzcon.data.projects.Data>)
                setupProjectRecyclerView()
                hideProgressLoader(binding.progressBar)

            }
            if (it.status == Resource.Status.LOADING) {
                showProgressLoader(binding.progressBar)

            }
            if (it.status == Resource.Status.ERROR) {
                hideProgressLoader(binding.progressBar)
                showToast(it.message.toString())
            }
        })
    }

    private fun setupProjectRecyclerView() {
        binding.recentProjectsRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = projectAdapter

        }
    }


    private fun setupBlogRecyclerView() {
        binding.recentBlogsRecyclerView.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = blogAdapter

        }
    }


    private fun setupServices() {
        serviceAdapter.addServices(getServices())
        binding.servicesRecyclerView.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = serviceAdapter
        }
    }

    private fun getServices() = arrayListOf(
        Services(
            R.drawable.ic_camera,
            getString(R.string.design_consultancy),
            getString(R.string.design_consultancy_desc)
        ),
        Services(
            R.drawable.ic_address,
            getString(R.string.interior_design_deco),
            getString(R.string.interior_desc)
        ),
        Services(
            R.drawable.ic_address,
            getString(R.string.turnkey_solution),
            getString(R.string.turnkey_desc)
        ),
        Services(
            R.drawable.ic_address,
            getString(R.string.gray_structure),
            getString(R.string.gray_structure_desc)
        ),
        Services(
            R.drawable.ic_address,
            getString(R.string.house_renovation),
            getString(R.string.renovation_desc)
        ),
        Services(
            R.drawable.ic_address,
            getString(R.string.real_estate),
            getString(R.string.real_estate_desc)
        )
    )


    override fun onServiceClicked(data: Services) {
        routeTo(R.id.action_homeFragment_to_serviceDetailFragment, bundleOf("service" to data))
    }

    override fun onBlogTapped(data: Data) {
        routeTo(R.id.action_homeFragment_to_blogDetailFragment, bundleOf("blog" to data))
    }

    override fun onProjectClicked(data: com.ovais.bizzcon.data.projects.Data) {
        routeTo(R.id.action_homeFragment_to_projectDetailsFragment, bundleOf("project" to data))
    }

    private fun routeToAllBlog() {
        binding.seeAllBlogs.setOnClickListener {
            routeTo(R.id.action_homeFragment_to_allBlogFragment)
        }
    }

    private fun routeToAllProjects() {
        binding.seeAllProjects.setOnClickListener {
            routeTo(R.id.action_homeFragment_to_allProjectFragment)
        }

    }
}