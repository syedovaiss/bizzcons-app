package com.ovais.bizzcon.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.ovais.bizzcon.apiservice.GeneralClient
import com.ovais.bizzcon.apiservice.HomeAPIService
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.config.network.NetworkClient
import retrofit2.Retrofit
import timber.log.Timber
import java.lang.Exception

/**
Created By Syed Ovais Akhtar On 5/18/21 10:28 PM2
 **/

class ProjectWorker(context: Context, workerParameters: WorkerParameters) :
    CoroutineWorker(context, workerParameters) {

    private var isSuccessful = false
    private lateinit var retrofit: Retrofit
    private var db = BizzconsDatabase(context)

    override suspend fun doWork(): Result {
        retrofit = NetworkClient.getInstance(Gson())
        return if (isTaskSuccessful()) {
            Result.success()
        } else {
            Result.retry()
        }
    }

    private suspend fun isTaskSuccessful(): Boolean {

        val service = getAPIService()
        return if (service.getRecentProjects().isSuccessful) {
            isSuccessful = true
            val data = service.getRecentProjects().body()!!.data
            try {
                db.projectDao().insert(data)

            } catch (e: Exception) {
                Timber.e(e)
            }
            isSuccessful

        } else {
            isSuccessful
        }


    }

    private fun getAPIService() = retrofit.create(HomeAPIService::class.java)
}