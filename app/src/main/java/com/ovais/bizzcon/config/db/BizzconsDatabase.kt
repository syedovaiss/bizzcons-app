package com.ovais.bizzcon.config.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ovais.bizzcon.config.dao.BlogDao
import com.ovais.bizzcon.config.dao.ProjectDao
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.utils.Constants.DATABASE.DATABASE_NAME
import com.ovais.bizzcon.utils.Constants.DATABASE.VERSION

/**
Created By Syed Ovais Akhtar On 4/30/21 1:58 PM2
 **/

@Database(
    entities = [Data::class, com.ovais.bizzcon.data.projects.Data::class],
    version = VERSION,
    exportSchema = false
)

abstract class BizzconsDatabase : RoomDatabase() {

    abstract fun blogDao(): BlogDao
    abstract fun projectDao(): ProjectDao

    companion object {
        @Volatile
        private var instance: BizzconsDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            BizzconsDatabase::class.java, DATABASE_NAME
        ).build()
    }

}
