package com.ovais.bizzcon.apiservice

import com.ovais.bizzcon.data.projects.Projects
import retrofit2.http.GET
import retrofit2.http.Path

/**
Created By Syed Ovais Akhtar On 5/21/21 6:24 PM2
 **/
interface ProjectApiService {

    @GET("api/projects/all/{pageNo}")
    suspend fun getPaginatedProjects(@Path("pageNo") position: Int): Projects
}