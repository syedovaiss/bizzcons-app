package com.ovais.bizzcon.callbacks

import com.ovais.bizzcon.features.services.model.Services

/**
Created By Syed Ovais Akhtar On 4/28/21 11:29 PM2
 **/
interface ServiceListener {

    fun onServiceClicked(data: Services)
}