package com.ovais.bizzcon.utils

/**
Created By Syed Ovais Akhtar On 3/31/21 6:52 PM2
 **/
object Constants {

    object NETWORK_CONSTANTS{

        const val BLOG_ENDPOINT = "api/blogs/all/1"
        const val PROJECTS_ENDPOINT = "api/projects/all/1"
        const val SERVICES_ENDPOINT = "api/services"
        const val unAuthorizeAccessMessage ="Full Authentication Required"
    }

    object DATABASE {
        const val VERSION = 1
        const val DATABASE_NAME = "bizzcons"
    }

    object PREFERENCES {
        const val FILENAME = "BIZZCONS_PREFERENCES"
        const val ON_BOARDING_FINISHED = "on_boarding_finished"
        const val TOKEN = "token"
        const val USER_DETAILS = "user_info"
        const val IS_LOGGED_IN = "is_logged_in"
    }

    object WORKER{
        const val TAG_BLOG = "worker_blog_tag"
        const val TAG_PROJECTS = "worker_project_tag"
    }

    const val STARTING_PAGE = 1
}