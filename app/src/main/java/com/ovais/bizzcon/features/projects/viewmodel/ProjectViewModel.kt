package com.ovais.bizzcon.features.projects.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ovais.bizzcon.data.projects.Data
import com.ovais.bizzcon.repositories.ProjectRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/21/21 6:23 PM2
 **/
class ProjectViewModel @ViewModelInject constructor(private val repository: ProjectRepository) : ViewModel() {

    /*  private var _projectList = MutableLiveData<List<Data>>()
      val projectList: LiveData<List<Data>> get() = _projectList*/

/*    fun requestProjects() {
        viewModelScope.launch {
            val allProjects:List<Data>
            withContext(Dispatchers.IO) {
                allProjects = repository.getProjects()
            }
            _projectList.value = allProjects
        }
    }*/

    private var currentSearchResult: Flow<PagingData<Data>>? = null

    fun getPaginatedBlog(): Flow<PagingData<Data>>? {

        val newResult: Flow<PagingData<Data>> = repository.getPaginatedProjects()
            .cachedIn(viewModelScope)
        currentSearchResult = newResult
        return currentSearchResult

    }
}