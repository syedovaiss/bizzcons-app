package com.ovais.bizzcon.repository

import android.content.Context
import androidx.paging.PagingData
import com.google.common.truth.Truth
import com.ovais.bizzcon.apiservice.ProjectApiService
import com.ovais.bizzcon.config.dao.BlogDao
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.data.projects.Data
import com.ovais.bizzcon.datasource.ProjectDataSource
import com.ovais.bizzcon.datasource.ProjectPagedSource
import com.ovais.bizzcon.repositories.ProjectRepository
import com.ovais.bizzcon.utils.TestUtils
import com.ovais.bizzcon.utils.getBlogDao
import com.ovais.bizzcon.utils.getDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
Created By Syed Ovais Akhtar On 5/25/21 5:04 PM2
 **/
@ExperimentalCoroutinesApi
class ProjectRepositoryTest {

    private lateinit var repository: ProjectRepository
    private lateinit var dataSource: ProjectDataSource
    private lateinit var pagingDataSource: ProjectPagedSource

    @Mock
    private lateinit var apiService: ProjectApiService

    @Mock
    private lateinit var context: Context
    private lateinit var db: BizzconsDatabase
    private lateinit var blogDao: BlogDao


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        setupDatabase()
        dataSource = ProjectDataSource(apiService, db)
        pagingDataSource = ProjectPagedSource(apiService)
        repository = ProjectRepository(dataSource, pagingDataSource)

    }

    private fun setupDatabase() {
        db = getDatabase(context)
        blogDao = getBlogDao(db)
    }

    @After
    fun tearDown() {
        db.close()
    }
    
    @Test
    fun `get paginated blog from data source`() = runBlockingTest {
        repository.getPaginatedProjects()
        val data = TestUtils.project
        val listOfBlog = listOf(data, data, data, data)
        val flow = flow<PagingData<List<Data>>> {}
        flow.collect {
            Truth.assertThat(repository.getPaginatedProjects()).isEqualTo(listOfBlog)

        }
    }
}