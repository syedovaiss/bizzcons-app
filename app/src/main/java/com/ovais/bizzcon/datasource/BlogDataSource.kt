package com.ovais.bizzcon.datasource

import com.ovais.bizzcon.apiservice.BlogApiService
import com.ovais.bizzcon.base.BaseResponse
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.data.blogs.Data
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/20/21 10:29 PM2
 **/
class BlogDataSource @Inject constructor(
    private val apiService: BlogApiService,
    private val db: BizzconsDatabase
) : BaseResponse() {

    suspend fun getBlogFromDatabase(): List<Data> = db.blogDao().getAllBlog()
}