package com.ovais.bizzcon.features.home.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.data.blogs.Blogs
import com.ovais.bizzcon.data.projects.Projects
import com.ovais.bizzcon.repositories.HomeRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
Created By Syed Ovais Akhtar On 4/23/21 12:19 AM2
 **/
class HomeViewModel @ViewModelInject constructor(private var repository: HomeRepository) : ViewModel() {

    private var _recentBlog = MutableLiveData<Resource<Blogs>>()

    val recentBlog: LiveData<Resource<Blogs>>
        get() = _recentBlog

    private var _recentProjects = MutableLiveData<Resource<Projects>>()

    val recentProjects: LiveData<Resource<Projects>>
        get() = _recentProjects

    fun requestBlog() {
        viewModelScope.launch {
            repository.getRecentBlog().collect {
                _recentBlog.value = it
            }
        }
    }

    fun requestProjects() {
        viewModelScope.launch {
            repository.getRecentProjects().collect {
                _recentProjects.value = it
            }
        }
    }
}