package com.ovais.bizzcon.features.intro.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ovais.bizzcon.callbacks.IntroListener
import com.ovais.bizzcon.databinding.IntroRowsBinding
import com.ovais.bizzcon.features.intro.IntroModel
import com.ovais.bizzcon.utils.show

/**
Created By Syed Ovais Akhtar On 4/17/21 11:43 PM2
 **/
class IntroAdapter(private var introListener: IntroListener) :
    RecyclerView.Adapter<IntroAdapter.IntroViewHolder>() {

    private var introItems = ArrayList<IntroModel>()

    fun addIntroItems(items: ArrayList<IntroModel>) {
        this.introItems = items
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroViewHolder {
        val view = IntroRowsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return IntroViewHolder(view)

    }

    override fun onBindViewHolder(holder: IntroViewHolder, position: Int) {
        val items = introItems[position]
        holder.bindData(items)

    }

    override fun getItemCount(): Int = introItems.size


    inner class IntroViewHolder(var itemRows: IntroRowsBinding) : RecyclerView.ViewHolder(itemRows.root) {

        fun bindData(items: IntroModel) {
            @Suppress("DEPRECATION")
            if (adapterPosition == 2) {
                itemRows.finishButton.show()
            }
            itemRows.data = items
            itemRows.animationView.setAnimation(items.anim)
            itemRows.finishButton.setOnClickListener {
                introListener.onFinished()
            }
        }


    }

}