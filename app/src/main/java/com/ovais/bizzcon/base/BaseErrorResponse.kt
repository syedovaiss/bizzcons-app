package com.ovais.bizzcon.base

data class BaseErrorResponse(
    val data: Any,
    val success: Boolean,
    val error: String
)