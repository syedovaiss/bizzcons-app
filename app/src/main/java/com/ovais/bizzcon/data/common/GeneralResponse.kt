package com.ovais.bizzcon.data.common

data class GeneralResponse(
    val error: String,
    val message: String,
    val success: Boolean
)