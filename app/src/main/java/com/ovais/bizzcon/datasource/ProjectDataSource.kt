package com.ovais.bizzcon.datasource

import com.ovais.bizzcon.apiservice.ProjectApiService
import com.ovais.bizzcon.base.BaseResponse
import com.ovais.bizzcon.config.db.BizzconsDatabase
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/21/21 6:24 PM2
 **/
class ProjectDataSource @Inject constructor(
    private val apiService: ProjectApiService,
    private val database: BizzconsDatabase
) : BaseResponse() {

    suspend fun getProjects() = database.projectDao().getAllProjects()
}