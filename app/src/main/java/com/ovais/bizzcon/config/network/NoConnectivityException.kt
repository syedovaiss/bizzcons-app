package com.ovais.bizzcon.config.network

import java.lang.Exception

/**
Created By Syed Ovais Akhtar On 3/31/21 7:50 PM2
 **/
class NoConnectivityException : Exception() {

    override val message
        get() = "Please check your network connection"
}