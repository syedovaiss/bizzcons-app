package com.ovais.bizzcon.callbacks

/**
Created By Syed Ovais Akhtar On 4/30/21 3:09 PM2
 **/
interface DialogListener {

    fun onOkay()
    fun onCancel()
}