package com.ovais.bizzcon.features.intro

import androidx.annotation.RawRes

/**
Created By Syed Ovais Akhtar On 4/18/21 12:07 AM2
 **/
data class IntroModel(var title: String, var description: String, @RawRes var anim: Int)