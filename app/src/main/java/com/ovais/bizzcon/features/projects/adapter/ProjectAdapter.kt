package com.ovais.bizzcon.features.projects.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.ovais.bizzcon.R
import com.ovais.bizzcon.callbacks.OnProjectClickListener
import com.ovais.bizzcon.data.projects.Data
import com.ovais.bizzcon.databinding.ItemRowsBinding
import com.ovais.bizzcon.utils.applyImage
import com.ovais.bizzcon.utils.applyResource
import com.ovais.bizzcon.utils.value
import kotlinx.android.synthetic.main.item_rows.view.*
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/20/21 5:05 PM2
 **/

class ProjectAdapter(private var listener: OnProjectClickListener) : RecyclerView.Adapter<ProjectAdapter.ProjectViewHolder>() {


    private val gson = Gson()
    private var listItems = ArrayList<Data>()

    fun addProjects(items: ArrayList<Data>) {
        this.listItems = items
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectViewHolder {
        val view = ItemRowsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProjectViewHolder(view)

    }

    override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) {
        val items = listItems[position]
        holder.bindData(items)

    }

    override fun getItemCount(): Int = listItems.size


    inner class ProjectViewHolder(var itemRows: ItemRowsBinding) : RecyclerView.ViewHolder(itemRows.root) {

        fun bindData(items: Data) {

            val imageList = gson.fromJson(items.project_image,ArrayList::class.java)
            if (imageList.isNotEmpty()){
                itemView.image.applyImage(itemView.context, imageList.first().toString())
            } else {
                itemView.image.applyResource(R.drawable.placeholder)
            }
            itemRows.title.value(items.project_title)
            itemRows.layout.setOnClickListener {
                listener.onProjectClicked(items)
            }

        }


    }

}