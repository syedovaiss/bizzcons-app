package com.ovais.bizzcon.utils

import java.text.SimpleDateFormat
import java.util.*

/**
Created By Syed Ovais Akhtar On 4/20/21 6:04 PM2
 **/
object DateTimeUtils {

    @JvmStatic
    fun getFormattedDateTime(epoch: Long, format: String): String {
        val formatter = SimpleDateFormat(format, Locale.getDefault())
        return formatter.format(epoch)
    }

}