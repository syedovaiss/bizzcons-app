package com.ovais.bizzcon.repositories

import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.data.common.GeneralResponse
import com.ovais.bizzcon.data.token.TokenResponse
import com.ovais.bizzcon.data.user.User
import com.ovais.bizzcon.data.user.UserResponse
import com.ovais.bizzcon.datasource.UserRemoteDataSource
import com.ovais.bizzcon.requestbody.TokenRequestBody
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 4/18/21 12:37 AM2
 **/
class UserRepository @Inject constructor(private var dataSource: UserRemoteDataSource) {

    suspend fun registerUser(user:User) = flow {
        emit(Resource.loading(null))
        emit(dataSource.registerUser(user))
    }

    suspend fun enqueueTokenRequest(email:String,password:String)  = flow{
        emit(Resource.loading(null))
        val makeTokenRequestBody = TokenRequestBody(email,password)
        emit(dataSource.enqueueTokenRequest(makeTokenRequestBody))
    }

    suspend fun getUserInfo() = flow {
        emit(Resource.loading(null))
        emit(dataSource.getUserInfo())
    }

}