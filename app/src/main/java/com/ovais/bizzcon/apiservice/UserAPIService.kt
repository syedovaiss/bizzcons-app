package com.ovais.bizzcon.apiservice

import com.ovais.bizzcon.data.common.GeneralResponse
import com.ovais.bizzcon.data.token.TokenResponse
import com.ovais.bizzcon.data.user.UserResponse
import com.ovais.bizzcon.requestbody.TokenRequestBody
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

/**
Created By Syed Ovais Akhtar On 4/18/21 12:38 AM2
 **/
interface UserAPIService {

    @Multipart
    @POST("api/public/user/signup")
    suspend fun registerUser(
        @Part("fullname") fullName: RequestBody,
        @Part("email") email: RequestBody,
        @Part("phone") phone: RequestBody,
        @Part("password") password: RequestBody,
        @Part profileImage: MultipartBody.Part?,
        @Part("address") address: RequestBody,
        @Part("dob") dob: RequestBody
    ): Response<GeneralResponse>


    @FormUrlEncoded
    @POST("api/public/user/")
    suspend fun enqueueTokenRequest(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<TokenResponse>

    @GET("api/user/")
    suspend fun getUserInfo() : Response<UserResponse>
}