package com.ovais.bizzcon.apiservice

import com.ovais.bizzcon.data.blogs.Blogs
import retrofit2.http.GET
import retrofit2.http.Path

/**
Created By Syed Ovais Akhtar On 5/20/21 10:29 PM2
 **/
interface BlogApiService {

    @GET("api/blogs/all/{pageNo}")
    suspend fun getPaginatedBlog(@Path("pageNo") position: Int):Blogs
}