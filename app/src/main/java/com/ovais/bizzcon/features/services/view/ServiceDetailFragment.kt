package com.ovais.bizzcon.features.services.view

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.databinding.FragmentServiceDetailsBinding
import com.ovais.bizzcon.extras.EmptyRepository
import com.ovais.bizzcon.features.services.model.Services

/**
Created By Syed Ovais Akhtar On 5/19/21 5:31 PM2
 **/
class ServiceDetailFragment:BaseFragment<FragmentServiceDetailsBinding>() {

    private lateinit var services:Services

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentServiceDetailsBinding  = FragmentServiceDetailsBinding.inflate(inflater,container,false)

    override fun onPostInit() {
        getServices()
    }

    private fun getServices() {
        arguments?.let {
            services =  it["service"] as Services
        }
        setupData(services)
    }

    private fun setupData(services: Services) {
        binding.services = services
    }
}