package com.ovais.bizzcon.repositories

import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.data.blogs.Blogs
import com.ovais.bizzcon.data.projects.Projects
import com.ovais.bizzcon.datasource.HomeDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

/**
Created By Syed Ovais Akhtar On 4/23/21 12:19 AM2
 **/
class HomeRepository(private var homeDataSource: HomeDataSource) {

    suspend fun getRecentBlog()= flow {
        emit(Resource.loading(null))
        emit(homeDataSource.getRecentBlog())
    }
    suspend fun getRecentProjects() = flow{
        emit(Resource.loading(null))
        emit(homeDataSource.getRecentProjects())
    }
}