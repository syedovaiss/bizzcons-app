package com.ovais.bizzcon.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.ovais.bizzcon.apiservice.HomeAPIService
import com.ovais.bizzcon.datasource.HomeDataSource
import com.ovais.bizzcon.repositories.HomeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
Created By Syed Ovais Akhtar On 5/25/21 5:06 PM2
 **/
@ExperimentalCoroutinesApi
class HomeRepositoryTest {
    private lateinit var repository: HomeRepository
    private lateinit var dataSource: HomeDataSource

    @Mock
    private lateinit var apiService: HomeAPIService

    @get:Rule
    var instantExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()
    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        dataSource = HomeDataSource(apiService)
        repository = HomeRepository(dataSource)

    }

    @Test
    fun `get recent blog`() = runBlockingTest {
        repository.getRecentBlog()
        assertThat(repository.getRecentBlog()).isEqualTo(dataSource.getRecentBlog())
    }

    @Test
    fun `get recent projects`() = runBlockingTest {
        repository.getRecentProjects()
        assertThat(repository.getRecentBlog()).isEqualTo(dataSource.getRecentProjects())
    }

}