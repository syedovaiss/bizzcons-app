package com.ovais.bizzcon.features.blogs.view

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.databinding.FragmentBlogDetailBinding
import com.ovais.bizzcon.extras.EmptyRepository
import com.ovais.bizzcon.utils.applyImage
import dagger.hilt.android.AndroidEntryPoint

/**
Created By Syed Ovais Akhtar On 5/19/21 5:52 PM2
 **/
@AndroidEntryPoint
class BlogDetailFragment : BaseFragment<FragmentBlogDetailBinding>() {

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentBlogDetailBinding = FragmentBlogDetailBinding.inflate(inflater, container, false)


    override fun onPostInit() {
        setupData()
    }

    private fun setupData() {
        binding.blogImage.applyImage(requireContext(),getBlog()!!.blog_header_image)
        binding.blog = getBlog()
    }

    private fun getBlog(): Data? {
        arguments?.let {
            return it["blog"] as Data
        }
        return null
    }
}