package com.ovais.bizzcon.utils

import android.content.Context
import androidx.room.Room
import com.ovais.bizzcon.config.db.BizzconsDatabase
import org.mockito.Mockito

/**
Created By Syed Ovais Akhtar On 5/25/21 4:34 PM2
 **/


fun getDatabase(context: Context): BizzconsDatabase = Room.inMemoryDatabaseBuilder(context, BizzconsDatabase::class.java).build()

fun getBlogDao(database: BizzconsDatabase) = database.blogDao()

fun getProjectDao(database: BizzconsDatabase) = database.projectDao()

fun <T> anyValue(): T = Mockito.any<T>()