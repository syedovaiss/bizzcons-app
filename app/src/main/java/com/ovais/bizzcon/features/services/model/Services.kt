package com.ovais.bizzcon.features.services.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import java.io.Serializable

/**
Created By Syed Ovais Akhtar On 4/28/21 11:26 PM2
 **/
data class Services(
    @DrawableRes var serviceImage: Int,
     var serviceName: String,
     var serviceDescription: String
):Serializable