package com.ovais.bizzcon.features.projects.view

import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.gson.Gson
import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.data.projects.Data
import com.ovais.bizzcon.databinding.ProjectDetailFragmentBinding
import com.ovais.bizzcon.extras.EmptyRepository
import com.ovais.bizzcon.utils.applyImage
import com.ovais.bizzcon.utils.applyResource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/20/21 5:17 PM2
 **/
@AndroidEntryPoint
class ProjectDetailsFragment :BaseFragment<ProjectDetailFragmentBinding>() {

    @Inject
    lateinit var gson: Gson
    private lateinit var projects:Data

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): ProjectDetailFragmentBinding  = ProjectDetailFragmentBinding.inflate(inflater,container,false)

    override fun onPostInit() {
        setupProjectDetails()
    }
    private fun setupProjectDetails() {
        arguments?.let {
            projects = it["project"] as Data
        }
        val imageList = gson.fromJson(projects.project_image,ArrayList::class.java)
        if (imageList.isNotEmpty()){
            binding.projectImage.applyImage(requireContext(), imageList.first().toString())
        } else {
            binding.projectImage.applyResource(R.drawable.placeholder)
        }
        binding.project = projects
    }
}