package com.ovais.bizzcon.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.ovais.bizzcon.apiservice.GeneralClient
import com.ovais.bizzcon.apiservice.HomeAPIService
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.config.network.NetworkClient
import retrofit2.Retrofit
import timber.log.Timber
import java.lang.Exception

/**
Created By Syed Ovais Akhtar On 5/18/21 10:14 PM2
 **/

class BlogWorker(context: Context, workerParameters: WorkerParameters) : CoroutineWorker(context, workerParameters) {

    private var isSuccessful = false
    private lateinit var retrofit: Retrofit
    private var db = BizzconsDatabase(context)

    override suspend fun doWork(): Result {
        retrofit = NetworkClient.getInstance(Gson())
        if (isTaskSuccessful()) {
            return Result.success()
        }
        return Result.retry()
    }

    private suspend fun isTaskSuccessful(): Boolean {

        val service = getAPIService()
        return if (service.getRecentBlog().isSuccessful) {
            isSuccessful = true
            try {
                val data = service.getRecentBlog().body()!!.data
                db.blogDao().insert(data)
            } catch (e: Exception) {
                Timber.e(e)
            }

            isSuccessful

        } else {
            isSuccessful
        }


    }

    private fun getAPIService() = retrofit.create(HomeAPIService::class.java)

}