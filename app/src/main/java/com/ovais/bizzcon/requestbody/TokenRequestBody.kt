package com.ovais.bizzcon.requestbody

/**
Created By Syed Ovais Akhtar On 5/18/21 5:18 PM2
 **/

data class TokenRequestBody(val email: String, val password: String)