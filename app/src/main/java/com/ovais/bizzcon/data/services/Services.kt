package com.ovais.bizzcon.data.services

data class Services(
    val `data`: List<Data>,
    val error: String,
    val success: Boolean
)