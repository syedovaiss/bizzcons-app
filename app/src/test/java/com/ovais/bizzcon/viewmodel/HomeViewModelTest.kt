package com.ovais.bizzcon.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.ovais.bizzcon.apiservice.HomeAPIService
import com.ovais.bizzcon.datasource.HomeDataSource
import com.ovais.bizzcon.features.home.viewmodel.HomeViewModel
import com.ovais.bizzcon.repositories.HomeRepository
import com.ovais.bizzcon.utils.LiveDataUtils.getOrAwaitValueTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
Created By Syed Ovais Akhtar On 5/25/21 4:30 PM2
 **/
@ExperimentalCoroutinesApi
class HomeViewModelTest {

    private lateinit var repository: HomeRepository
    private lateinit var dataSource: HomeDataSource

    @Mock
    private lateinit var apiService: HomeAPIService

    private lateinit var viewModel: HomeViewModel

    @get:Rule
    var instantExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()
    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        dataSource = HomeDataSource(apiService)
        repository = HomeRepository(dataSource)
        viewModel = HomeViewModel(repository)

    }

    @Test
    fun `request blogs`() = runBlockingTest {
        viewModel.requestBlog()
        val liveData = viewModel.recentBlog.getOrAwaitValueTest()
        val blog = repository.getRecentBlog()
        assertThat(blog).isEqualTo(liveData)
    }

    @Test
    fun `request projects`() = runBlockingTest {
        viewModel.requestProjects()
        val liveData = viewModel.recentProjects.getOrAwaitValueTest()
        val projects = repository.getRecentProjects()
        assertThat(projects).isEqualTo(liveData)
    }

}