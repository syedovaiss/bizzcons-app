package com.ovais.bizzcon.viewmodel

import android.content.Context
import androidx.paging.PagingData
import com.google.common.truth.Truth.assertThat
import com.ovais.bizzcon.apiservice.BlogApiService
import com.ovais.bizzcon.config.dao.BlogDao
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.datasource.BlogDataSource
import com.ovais.bizzcon.datasource.BlogPagingSource
import com.ovais.bizzcon.features.blogs.viewmodel.BlogViewModel
import com.ovais.bizzcon.repositories.BlogRepository
import com.ovais.bizzcon.utils.TestUtils
import com.ovais.bizzcon.utils.getBlogDao
import com.ovais.bizzcon.utils.getDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
Created By Syed Ovais Akhtar On 5/25/21 2:55 PM2
 **/
@ExperimentalCoroutinesApi
class BlogViewModelTest {

    private lateinit var repository: BlogRepository
    private lateinit var dataSource: BlogDataSource
    private lateinit var pagingDataSource: BlogPagingSource

    @Mock
    private lateinit var apiService: BlogApiService

    @Mock
    private lateinit var context: Context
    private lateinit var db: BizzconsDatabase
    private lateinit var blogDao: BlogDao
    private lateinit var viewModel: BlogViewModel


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        setupDatabase()
        dataSource = BlogDataSource(apiService, db)
        pagingDataSource = BlogPagingSource(apiService)
        repository = BlogRepository(dataSource, pagingDataSource)
        viewModel = BlogViewModel(repository)

    }

    private fun setupDatabase() {
        db = getDatabase(context)
        blogDao = getBlogDao(db)
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun `get paginated blog`() = runBlocking {
        val data = TestUtils.blog
        val listOfBlog = listOf(data, data, data, data)
        val flow = flow<PagingData<List<Data>>> {}
        flow.collect {
            assertThat(repository.getPaginatedBlog()).isEqualTo(listOfBlog)

        }
    }
}