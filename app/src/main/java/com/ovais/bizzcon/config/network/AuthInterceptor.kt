package com.ovais.bizzcon.config.network

import com.ovais.bizzcon.events.LogoutEvent
import com.ovais.bizzcon.preferences.PreferenceHelper
import okhttp3.Interceptor
import okhttp3.Response
import org.greenrobot.eventbus.EventBus

/**
Created By Syed Ovais Akhtar On 3/31/21 6:50 PM2
 **/
class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val token = PreferenceHelper.getToken()
        var authToken =""
        if (token.isNullOrBlank()){
            EventBus.getDefault().post(LogoutEvent())
        } else {
            authToken = "Bearer $token"
        }

        val authRequest = request.newBuilder().addHeader("Authorization",authToken).build()
        return chain.proceed(authRequest)
    }

}