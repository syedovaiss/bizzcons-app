package com.ovais.bizzcon.features.splash.view

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.callbacks.DialogListener
import com.ovais.bizzcon.databinding.FragmentSplashBinding
import com.ovais.bizzcon.datasource.SplashDataSource
import com.ovais.bizzcon.features.splash.viewmodel.SplashViewModel
import com.ovais.bizzcon.preferences.PreferenceHelper
import com.ovais.bizzcon.repositories.SplashRepository
import com.ovais.bizzcon.utils.Constants.PREFERENCES.IS_LOGGED_IN
import com.ovais.bizzcon.utils.Constants.PREFERENCES.ON_BOARDING_FINISHED
import com.ovais.bizzcon.utils.routeTo
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(),DialogListener {

    @Inject
    lateinit var gson: Gson
    private var isError = false

    private val viewModel: SplashViewModel by viewModels()

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSplashBinding = FragmentSplashBinding.inflate(inflater, container, false)

    override fun onPostInit() {
        initListeners(this)
        /*getBlog()
        getProjects()
        getServices()*/
        setupObservers()
    }

    private fun setupObservers() {
        setupBlogObserver()
        setupProjectsObserver()
        enableScreenTimeout(3000)

    }

    private fun getBlog() = viewModel.getBlog()

    private fun getProjects() = viewModel.getProjects()

    private fun getServices() = viewModel.getServices()

    private fun setupBlogObserver() {

        viewModel.blogResponse.observe(viewLifecycleOwner, Observer {
            if (it.status == Resource.Status.SUCCESS) {
                viewModel.insertBlog(it?.data!!.data)
            }
            if(it.status == Resource.Status.ERROR){
                isError  = true
            }
        })
    }

    private fun setupProjectsObserver() {
        viewModel.projectResponse.observe(viewLifecycleOwner, Observer {
            if (it.status == Resource.Status.SUCCESS) {
                viewModel.insertProjects(it?.data!!.data)
            }
            if(it.status == Resource.Status.ERROR){
                showDialog(getString(R.string.something_went_wrong),getString(R.string.use_offline_mode))
            }
        })
    }

    private fun enableScreenTimeout(@Suppress("SameParameterValue") millisSeconds: Long) =
        Handler(Looper.getMainLooper()).postDelayed({ handleIntents() }, millisSeconds)

    private fun handleIntents() {
        if (isOnBoardingFinished() && isLoggedIn()) {
            routeTo(R.id.action_splashFragment_to_homeFragment)
        } else if (isOnBoardingFinished()) {
            routeTo(R.id.action_splashFragment_to_loginFragment)
        } else {
            routeTo(R.id.action_splashFragment_to_introFragment)
        }
    }

    override fun onCancel() {
        requireActivity().finishAffinity()
    }

    override fun onOkay() {
        dismiss()
    }

    private fun isOnBoardingFinished() = PreferenceHelper.getBoolean(ON_BOARDING_FINISHED)

    private fun isLoggedIn() = PreferenceHelper.getBoolean(IS_LOGGED_IN)

}