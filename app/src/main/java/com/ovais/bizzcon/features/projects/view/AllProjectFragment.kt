package com.ovais.bizzcon.features.projects.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.callbacks.OnProjectClickListener
import com.ovais.bizzcon.data.projects.Data
import com.ovais.bizzcon.databinding.FragmentAllProjectsBinding
import com.ovais.bizzcon.datasource.ProjectDataSource
import com.ovais.bizzcon.datasource.ProjectPagedSource
import com.ovais.bizzcon.features.projects.adapter.ProjectPagedAdapter
import com.ovais.bizzcon.features.projects.viewmodel.ProjectViewModel
import com.ovais.bizzcon.repositories.ProjectRepository
import com.ovais.bizzcon.utils.hide
import com.ovais.bizzcon.utils.init
import com.ovais.bizzcon.utils.routeTo
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/20/21 5:27 PM2
 **/

@AndroidEntryPoint
class AllProjectFragment : BaseFragment<FragmentAllProjectsBinding>(), OnProjectClickListener {

    private val viewModel: ProjectViewModel by viewModels()
    private lateinit var projectAdapter: ProjectPagedAdapter

    @Inject
    lateinit var dataSource: ProjectDataSource

    @Inject
    lateinit var pagedDataSource: ProjectPagedSource

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAllProjectsBinding = FragmentAllProjectsBinding.inflate(inflater, container, false)

    override fun onPostInit() {
        setupAdapter()
        setupData()
    }

    private fun setupAdapter() {
        projectAdapter = ProjectPagedAdapter(this)
    }

    override fun onProjectClicked(data: Data) {
        routeTo(R.id.action_allProjectFragment_to_projectDetailsFragment, bundleOf("project" to data))

    }

    private fun setupData() {
        lifecycleScope.launch {
            viewModel.getPaginatedBlog()?.let {
                it.collectLatest {
                    binding.progressBar.hide()
                    projectAdapter.submitData(it)
                }
            }
        }
        setupRecyclerView()


    }

    private fun setupRecyclerView() {
        binding.allProjectRV.init(requireContext(), projectAdapter)

    }
}