package com.ovais.bizzcon.features.splash.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.data.blogs.Blogs
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.data.projects.Projects
import com.ovais.bizzcon.data.services.Services
import com.ovais.bizzcon.repositories.SplashRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
Created By Syed Ovais Akhtar On 3/31/21 7:00 PM2
 **/

class SplashViewModel @ViewModelInject constructor(private val repository: SplashRepository) : ViewModel() {

    private var _blogResponse = MutableLiveData<Resource<Blogs>>()

    val blogResponse: LiveData<Resource<Blogs>>
        get() = _blogResponse

    private var _projectsResponse = MutableLiveData<Resource<Projects>>()

    val projectResponse: LiveData<Resource<Projects>>
        get() = _projectsResponse


    private var _servicesResponse = MutableLiveData<Resource<Services>>()

    val servicesResponse: LiveData<Resource<Services>>
        get() = _servicesResponse


    fun getBlog() {
        viewModelScope.launch {
            repository.getBlog().collect {
                _blogResponse.value = it
            }
        }

    }

    fun getProjects() {
        viewModelScope.launch {
            repository.getProjects().collect {
                _projectsResponse.value = it
            }
        }
    }

    fun getServices() {
        viewModelScope.launch {
            repository.getServices().collect {
                _servicesResponse.value = it
            }
        }
    }

    fun insertBlog(blog: List<Data>) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.insertBlog(blog)
            }
        }
    }

    fun insertProjects(projects: List<com.ovais.bizzcon.data.projects.Data>) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.insertProjects(projects)
            }
        }
    }
}