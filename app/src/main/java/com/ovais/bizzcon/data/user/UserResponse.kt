package com.ovais.bizzcon.data.user

data class UserResponse(
    val `data`: Data,
    val error: String,
    val success: Boolean
)