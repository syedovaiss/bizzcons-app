package com.ovais.bizzcon.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.data.projects.Data
import com.ovais.bizzcon.datasource.ProjectDataSource
import com.ovais.bizzcon.datasource.ProjectPagedSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/21/21 6:23 PM2
 **/
class ProjectRepository @Inject constructor(
    private val dataSource: ProjectDataSource,
    private val pagingDataSource: ProjectPagedSource
)  {

    suspend fun getProjects() = flow {
        emit(Resource.loading(null))
        emit(dataSource.getProjects())
    }

    fun getPaginatedProjects(): Flow<PagingData<Data>> = Pager(
        config = PagingConfig(
            pageSize = 10,
            initialLoadSize = 10,
            prefetchDistance = 10,
            enablePlaceholders = false
        ),
        pagingSourceFactory = { pagingDataSource }
    ).flow

}