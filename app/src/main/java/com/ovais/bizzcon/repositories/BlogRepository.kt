package com.ovais.bizzcon.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.ovais.bizzcon.data.blogs.Data
import com.ovais.bizzcon.datasource.BlogDataSource
import com.ovais.bizzcon.datasource.BlogPagingSource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 5/20/21 10:29 PM2
 **/
class BlogRepository @Inject constructor(
    private val dataSource: BlogDataSource,
    private val pagingDataSource: BlogPagingSource
)  {

  /*  suspend fun getBlog() = dataSource.getBlogFromDatabase()*/

    fun getPaginatedBlog(): Flow<PagingData<Data>> = Pager(
        config = PagingConfig(
            pageSize = 10,
            initialLoadSize = 10,
            prefetchDistance = 10,
            enablePlaceholders = false
        ),
        pagingSourceFactory = { pagingDataSource }
    ).flow

}