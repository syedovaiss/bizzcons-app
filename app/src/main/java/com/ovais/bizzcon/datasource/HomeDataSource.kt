package com.ovais.bizzcon.datasource

import com.ovais.bizzcon.apiservice.HomeAPIService
import com.ovais.bizzcon.base.BaseResponse

/**
Created By Syed Ovais Akhtar On 4/23/21 12:19 AM2
 **/
class HomeDataSource(private var apiService: HomeAPIService) : BaseResponse() {

    suspend fun getRecentBlog() = getResponse {
        apiService.getRecentBlog()
    }

    suspend fun getRecentProjects() = getResponse {
        apiService.getRecentProjects()
    }
}