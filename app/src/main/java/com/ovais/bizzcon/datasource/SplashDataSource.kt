package com.ovais.bizzcon.datasource

import com.ovais.bizzcon.apiservice.GeneralClient
import com.ovais.bizzcon.base.BaseResponse
import com.ovais.bizzcon.base.Resource
import com.ovais.bizzcon.config.db.BizzconsDatabase
import com.ovais.bizzcon.data.blogs.Blogs
import com.ovais.bizzcon.data.blogs.Data
import javax.inject.Inject

/**
Created By Syed Ovais Akhtar On 3/31/21 7:32 PM2
 **/

class SplashDataSource @Inject constructor(
    private val apiService: GeneralClient,
    private var database: BizzconsDatabase
) : BaseResponse() {


    suspend fun getBlogs(): Resource<Blogs> = getResponse {
        apiService.getBlog()
    }

    suspend fun getProjects() = getResponse {
        apiService.getProjects()
    }

    suspend fun getServices() = getResponse {
        apiService.getServices()
    }

    suspend fun insertBlog(data: List<Data>){
        database.blogDao().insert(data)
    }

    suspend fun insertProjects(data: List<com.ovais.bizzcon.data.projects.Data>){
        database.projectDao().insert(data)
    }
}