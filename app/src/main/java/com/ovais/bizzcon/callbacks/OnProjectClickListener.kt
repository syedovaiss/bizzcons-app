package com.ovais.bizzcon.callbacks

import com.ovais.bizzcon.data.projects.Data

/**
Created By Syed Ovais Akhtar On 5/20/21 5:06 PM2
 **/
interface OnProjectClickListener {
    fun onProjectClicked(data:Data)
}