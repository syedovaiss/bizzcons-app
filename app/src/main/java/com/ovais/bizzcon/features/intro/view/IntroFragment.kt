package com.ovais.bizzcon.features.intro.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.ovais.bizzcon.R
import com.ovais.bizzcon.base.BaseFragment
import com.ovais.bizzcon.callbacks.IntroListener
import com.ovais.bizzcon.databinding.FragmentIntroBinding
import com.ovais.bizzcon.extras.EmptyRepository
import com.ovais.bizzcon.features.intro.IntroModel
import com.ovais.bizzcon.features.intro.adapter.IntroAdapter
import com.ovais.bizzcon.preferences.PreferenceHelper
import com.ovais.bizzcon.utils.Constants.PREFERENCES.ON_BOARDING_FINISHED
import com.ovais.bizzcon.utils.routeTo
import com.ovais.bizzcon.utils.showToast

class IntroFragment : BaseFragment<FragmentIntroBinding>(), IntroListener {

    private lateinit var introAdapter: IntroAdapter
    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentIntroBinding = FragmentIntroBinding.inflate(inflater, container, false)

    override fun onPostInit() {
        introAdapter = IntroAdapter(this)
        setupRecyclerView()
    }
    private fun setupRecyclerView() {
        introAdapter.addIntroItems(setupData())
        binding.introRecyclerView.apply {
            adapter = introAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }
    }

    private fun setupData(): ArrayList<IntroModel> {
        val introModelOne = IntroModel("Title No 01", getString(R.string.dummy_text), R.raw.people)
        val introModelTwo = IntroModel("Title No 02", getString(R.string.dummy_text), R.raw.teamwork)
        val introModelThree = IntroModel("Title No 03", getString(R.string.dummy_text), R.raw.get_a_quote)
        return arrayListOf(introModelOne, introModelTwo, introModelThree)
    }

    override fun onFinished() {
        PreferenceHelper.putBoolean(ON_BOARDING_FINISHED,true)
        routeTo(R.id.action_introFragment_to_loginFragment)
    }
}